package com.mycompany.zsiklimatyzator;

import java.awt.*;
import java.awt.event.*;
import java.text.DecimalFormat;
import javax.swing.*;
import org.jfree.chart.ChartPanel;

public class MyFrame extends JFrame implements ActionListener {
    
    private JPanel north,center,south,centerLeft,centerRight;
    private JLabel startTempLabel,tempChangeLabel,finalTempLabel,currentTemp,currentHeat,currentChange,timeLabel;
    private JTextArea startTemp,tempChange,finalTemp;
    private JButton startButton,setButton,graphButton;
    private JComboBox comboBoxLocation,comboBoxScenario;
    private Timer timer;
    private Heater heater = new Heater();
    private Charts chartT = new Charts("Obecna Temperatura", "Temperatura [°C]",Color.RED);
    private Charts chartH = new Charts("Poziom ogrzewania", "Poziom ogrzewania [%]",Color.MAGENTA);
    private Charts chartC = new Charts("Zmiana temperatury", "Zmiana temperatury [°C]",Color.BLUE);
    private ChartPanel chartPanel1,chartPanel2,chartPanel3;
    
    private int[] jesien = new int[]{3,3,4,5,5,4,4,4,3,4,4,5,5,6,6,5,4,4,3,3,3,2,2,2}; 
    private int[] zima = new int[]{0,0,0,-1,-1,-2,-1,-1,-1,-1,-1,0,0,1,1,1,2,1,1,0,0,0,0,0}; 
    private int[] wiosna = new int[]{4,4,4,4,4,4,5,6,7,8,10,11,11,12,13,13,12,10,9,8,7,6,5,5}; 
    private int[] lato = new int[]{16,16,15,15,16,17,19,21,23,26,26,27,27,28,28,27,25,23,21,20,20,19,18,17}; 
    private int counterH=12, counterM=0;
    
    public MyFrame() {	
	super("Inteligenty Klimatyzator");
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            setSize(700,525);
            setResizable( false );
            setLocationRelativeTo(null);
            setLayout(new BorderLayout());  
            
            //NORTH PANEL
            north = new JPanel();
            startTempLabel = new JLabel("Początkowa temperatura:");
            startTemp = new JTextArea("00",1,2);
            tempChangeLabel = new JLabel("Temperatura na zewnątrz:");
            tempChange = new JTextArea("00",1,3);
            finalTempLabel = new JLabel("Docelowa temperatura:");
            finalTemp = new JTextArea("00",1,2);
            setButton = new JButton("Set");
            setButton.addActionListener(this);
            startButton = new JButton("Start");
            startButton.addActionListener(this);
            
            north.add(startTempLabel);
            north.add(startTemp);
            north.add(tempChangeLabel);
            north.add(tempChange);
            north.add(finalTempLabel);
            north.add(finalTemp);
            north.add(setButton);
            north.add(startButton);
            
            //CENTER PANEL
            center = new JPanel();
            center.setLayout(new GridLayout(1,2));
            
            centerLeft = new JPanel(new GridLayout(4,1));
            
            JLabel cTl = new JLabel("Obecna temperatura");
            cTl.setHorizontalAlignment(JLabel.CENTER);
            
            currentTemp = new JLabel("00");
            currentTemp.setFont(new Font("Verdana", Font.PLAIN, 125));
            currentTemp.setHorizontalAlignment(JLabel.CENTER);
            currentTemp.setVerticalAlignment(JLabel.CENTER);
            
            centerLeft.add(cTl);
            centerLeft.add(new JLabel());
            centerLeft.add(currentTemp);
            centerLeft.setBorder(BorderFactory.createLineBorder(Color.black));
            
            centerRight = new JPanel(new GridLayout(4,1));
            
            JLabel cTh = new JLabel("Poziom ogrzewania");
            cTh.setHorizontalAlignment(JLabel.CENTER);
            
            currentHeat = new JLabel("0");
            currentHeat.setFont(new Font("Verdana", Font.PLAIN, 75));
            currentHeat.setHorizontalAlignment(JLabel.CENTER);
            currentHeat.setVerticalAlignment(JLabel.CENTER);
            
            JLabel cTc = new JLabel("Zmiana temperatury");
            cTc.setHorizontalAlignment(JLabel.CENTER);
            
            currentChange = new JLabel("00");
            currentChange.setFont(new Font("Verdana", Font.PLAIN, 75));
            currentChange.setHorizontalAlignment(JLabel.CENTER);
            currentChange.setVerticalAlignment(JLabel.CENTER);
            
            centerRight.add(cTh);
            centerRight.add(currentHeat);
            centerRight.setBorder(BorderFactory.createLineBorder(Color.black));
            centerRight.add(cTc);
            centerRight.add(currentChange);
            
            center.add(centerLeft);
            center.add(centerRight);
            
            //SOUTH PANEL
            south = new JPanel();
            
            comboBoxLocation = new JComboBox();
            comboBoxLocation.addItem("--");
            comboBoxLocation.addItem("Budynek ocieplany");
            comboBoxLocation.addItem("Budynek nieocieplany");
            comboBoxLocation.addItem("Kontener metalowy");
            
            comboBoxScenario = new JComboBox();
            comboBoxScenario.addItem("--");
            comboBoxScenario.addItem("Zima");
            comboBoxScenario.addItem("Wiosna");
            comboBoxScenario.addItem("Lato");
            comboBoxScenario.addItem("Jesień");
            
            timeLabel = new JLabel();
            
            graphButton = new JButton("Pokaż wykresy");
            graphButton.addActionListener(this);
            
            south.add(comboBoxLocation);
            south.add(comboBoxScenario);
            south.add(timeLabel);
            south.add(graphButton);
            
            chartPanel1 = new ChartPanel(chartT.drawChart());
            chartPanel2 = new ChartPanel(chartH.drawChart());
            chartPanel3 = new ChartPanel(chartC.drawChart());
            
            add(center, BorderLayout.CENTER);
            add(north, BorderLayout.NORTH);
            add(south, BorderLayout.SOUTH);
            setVisible(true);
            
            timer = new Timer(1000, new ActionListener() {
	    	    @Override
	    	    public void actionPerformed(ActionEvent ae) {
                        heater.gen();
                        chartT.addSeries(heater.getCurrentTemp());
                        chartH.addSeries(heater.getHeatLevel());
                        chartC.addSeries(heater.getCurrentTempChange());
                        currentHeat.setText(Math.round(heater.getHeatLevel())+"");
                        currentTemp.setText(Math.round(heater.getCurrentTemp()*10.0)/10.0+"");
                        String tempS = String.format("%.3f", heater.getCurrentTempChange());
                        currentChange.setText(tempS);
                        heater.setLocation(comboBoxLocation.getSelectedIndex());
                        heater.update();
                        
                        double temp = 0;
                        if(comboBoxScenario.getSelectedIndex()==0){
                            temp=Double.parseDouble(tempChange.getText());
                            tempChange.setEditable(true);
                            timeLabel.setText("");
                        }else if(comboBoxScenario.getSelectedIndex()==1){
                            temp=zima[counterH];
                            tempChange.setText(temp+"");
                            timeLabel.setText(counterH+":"+new DecimalFormat("00").format(counterM));
                            tempChange.setEditable(false);
                        }else if(comboBoxScenario.getSelectedIndex()==2){
                            temp=wiosna[counterH];
                            tempChange.setText(temp+"");
                            timeLabel.setText(counterH+":"+new DecimalFormat("00").format(counterM));
                            tempChange.setEditable(false);
                        }else if(comboBoxScenario.getSelectedIndex()==3){
                            temp=lato[counterH];
                            tempChange.setText(temp+"");
                            timeLabel.setText(counterH+":"+new DecimalFormat("00").format(counterM));
                            tempChange.setEditable(false);
                        }else if(comboBoxScenario.getSelectedIndex()==4){
                            temp=jesien[counterH];
                            tempChange.setText(temp+"");
                            timeLabel.setText(counterH+":"+new DecimalFormat("00").format(counterM));
                            tempChange.setEditable(false);
                        }
                        startTemp.setText(currentTemp.getText());
                        heater.setTempChange(temp);
                        if(counterM==59){
                            counterM=0;
                            if(counterH==23){
                                counterH=0;
                            }else{
                                counterH++;
                            }
                        }else{
                            counterM++;
                        }
                    }
            });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String s = e.getActionCommand();
        if (s.equals("Start")) { 
            startButton.setText("Stop");
            startTemp.setEditable(false);
            timer.restart();
        }
        if (s.equals("Stop")) {
            startButton.setText("Start");
            startTemp.setEditable(true);
            timer.stop();
        }
        if (s.equals("Set")) {
            double temp = 0;
            if(comboBoxScenario.getSelectedIndex()==0){
                temp=Double.parseDouble(tempChange.getText());
            }else if(comboBoxScenario.getSelectedIndex()==1){
                temp=zima[counterH];
            }else if(comboBoxScenario.getSelectedIndex()==2){
                temp=wiosna[counterH];
            }else if(comboBoxScenario.getSelectedIndex()==3){
                temp=lato[counterH];
            }else if(comboBoxScenario.getSelectedIndex()==4){
                temp=jesien[counterH];
            }
            heater.setStart(Double.parseDouble(startTemp.getText()), temp, Double.parseDouble(finalTemp.getText()));
        }
        if (s.equals("Pokaż wykresy")) {
            JDialog graphs = new JDialog(this, "Wykresy"); 
                graphs.setSize(520,800);
                graphs.setResizable(true);
                graphs.setLocationRelativeTo(null);
                graphs.setVisible(true);
                JPanel graphPanel = new JPanel(new GridLayout(3,1));
                graphPanel.add(chartPanel1);
                graphPanel.add(chartPanel2);
                graphPanel.add(chartPanel3);
                graphs.setSize(525,800);
                graphs.add(graphPanel);
        }
    }
}