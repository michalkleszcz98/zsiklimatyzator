package com.mycompany.zsiklimatyzator;

import java.awt.BasicStroke;
import java.awt.Color;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;


public class Charts{
    
    private XYSeries series;
    private XYSeriesCollection dataset;
    private String name,yseries;
    private int x;
    private Color color;
    
    public Charts(String n, String y, Color c) {
        x=0;
        name=n;
        yseries=y;
        color=c;
        series = new XYSeries(n);
        series.setMaximumItemCount(120);
        dataset = new XYSeriesCollection();
        dataset.addSeries(series);
        
    }
    
    public void addSeries(double y) {
        series.add(x, y);
        x++;
    }

    public JFreeChart drawChart() {
        JFreeChart chart = ChartFactory.createXYLineChart(name,"Czas [s]",yseries,dataset,PlotOrientation.VERTICAL,false,false,false);
        XYItemRenderer rend = chart.getXYPlot().getRenderer();
        rend.setSeriesStroke (0, new BasicStroke(2.5f));
        rend.setSeriesPaint(0, color);
    return chart;
    }
}
