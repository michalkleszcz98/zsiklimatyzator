package com.mycompany.zsiklimatyzator;

public class Heater {
    
    private int location=0;
    private double heatLevel;
    private double startTemp,tempChange,targetTemp,x;
    private double currentTemp,currentTempChange;
    
    private double[][] temperature = new double[6][501];
    private double[][] heater = new double[6][1601];
    private double[][] value = new double[2][1601];

    public void setStart(double sT,double tC, double tT) {
        startTemp=sT;
        tempChange=tC;
        if(tT-sT>0) {
            targetTemp=tT+0.15;
        }else{
            targetTemp=tT;
        }   
        currentTemp=startTemp;
    }
    
    public void gen() {
        
        //TEMPERATURE
        for(int i=0; i<501; i++) {
            temperature[0][i]=(i*0.1);
            
            //ZZ
            if ((temperature[0][i]>=0)&(temperature[0][i]<=targetTemp-7)) {
                temperature[1][i]=1;
            } else if ((temperature[0][i]>targetTemp-7)&(temperature[0][i]<targetTemp-5)) {
                temperature[1][i]=((targetTemp-5)-temperature[0][i])/((targetTemp-5)-(targetTemp-7));
            } else {
                temperature[1][i]=0;
            }
            
            //System.out.println(temperature[1][i]);
            
            //Z
            if ((temperature[0][i]>targetTemp-7)&(temperature[0][i]<=targetTemp-4.5)) {
                temperature[2][i]=(temperature[0][i]-(targetTemp-7))/((targetTemp-4.5)-(targetTemp-7));
            } else if ((temperature[0][i]>targetTemp-4.5)&(temperature[0][i]<targetTemp)) {
                temperature[2][i]=((targetTemp)-temperature[0][i])/((targetTemp)-(targetTemp-4.5));
            } else {
                temperature[2][i]=0;
            }
            
            //System.out.println(temperature[2][i]);
            
            //N
            if ((temperature[0][i]>targetTemp-4)&(temperature[0][i]<targetTemp)) {
                temperature[3][i]=(temperature[0][i]-(targetTemp-4))/((targetTemp)-(targetTemp-4));
            } else if ((temperature[0][i]>=targetTemp)&(temperature[0][i]<targetTemp+4)) {
                temperature[3][i]=((targetTemp+4)-temperature[0][i])/((targetTemp+4)-(targetTemp));
            } else {
                temperature[3][i]=0;
            }
            
            //System.out.println(temperature[3][i]);
            
            //G
            if ((temperature[0][i]>targetTemp)&(temperature[0][i]<=targetTemp+4.5)) {
                temperature[4][i]=(temperature[0][i]-(targetTemp))/((targetTemp+4.5)-(targetTemp));
            } else if ((temperature[0][i]>targetTemp+4.5)&(temperature[0][i]<targetTemp+7)) {
                temperature[4][i]=((targetTemp+7)-temperature[0][i])/((targetTemp+7)-(targetTemp+4.5));
            } else {
                temperature[4][i]=0;
            }
            
            //System.out.println(temperature[4][i]);
            
            //ZG
            if ((temperature[0][i]>targetTemp+5)&(temperature[0][i]<targetTemp+8)) {
                temperature[5][i]=(temperature[0][i]-(targetTemp+5))/((targetTemp+8)-(targetTemp+5));
            } else if (temperature[0][i]>=targetTemp+8) {
                temperature[5][i]=1;
            } else {
                temperature[5][i]=0;
            }
        }
            
            //System.out.println(temperature[5][i]);
            //System.out.println(temperature[0][i]+" "+temperature[2][i]);
        for(int i=0; i<1601; i++) {
            //HEATER
            heater[0][i]=(i*0.2)-160;
            
            //NVC
            if ((heater[0][i]>-160)&(heater[0][i]<=-120)) {
                heater[1][i]=(heater[0][i]+160)/(-120+160);
            } else if ((heater[0][i]>-120)&(heater[0][i]<=-80)) {
                heater[1][i]=1;
            } else if ((heater[0][i]>-80)&(heater[0][i]<-40)) {
                heater[1][i]=(-40-heater[0][i])/(-40+80);
            } else {
                heater[1][i]=0;
            }
            
            //NC
            if ((heater[0][i]>-80)&(heater[0][i]<=-40)) {
               heater[2][i]=(heater[0][i]+80)/(-40+80);
            } else if ((heater[0][i]>-40)&(heater[0][i]<0)) {
               heater[2][i]=(0-heater[0][i])/(0+40);
            } else {
               heater[2][i]=0;
            }
            
            //Z
            if ((heater[0][i]>-40)&(heater[0][i]<=0)) {
               heater[3][i]=(heater[0][i]+40)/(0+40);
            } else if ((heater[0][i]>0)&(heater[0][i]<40)) {
               heater[3][i]=(40-heater[0][i])/(40-0);
            } else {
               heater[3][i]=0;
            }
            
            //PG
            if ((heater[0][i]>0)&(heater[0][i]<=40)) {
               heater[4][i]=(heater[0][i]-00)/(40-0);
            } else if ((heater[0][i]>40)&(heater[0][i]<80)) {
               heater[4][i]=(80-heater[0][i])/(80-40);
            } else {
               heater[4][i]=0;
            }
            
            //PGM
            if ((heater[0][i]>40)&(heater[0][i]<80)) {
                heater[5][i]=(heater[0][i]-40)/(80-40);
            } else if ((heater[0][i]>=80)&(heater[0][i]<120)) {
                heater[5][i]=1;
            } else if ((heater[0][i]>=120)&(heater[0][i]<160)) {
                heater[5][i]=(160-heater[0][i])/(160-120);             
            } else {
                heater[5][i]=0;
            }
        }
        
        double R1,R2,R3,R4,R5;
        for(int i=0;i<1601;i++) {
            value[0][i]=((i*0.2)-160);
            //R1
            if (temperature[1][(int)(currentTemp*10+1)]>0) {
                R1=Math.min(heater[5][i],temperature[1][(int)(currentTemp*10+1)]);
            } else {
                R1=0;
            }
            //R2
            if (temperature[2][(int)(currentTemp*10+1)]>0) {
                R2=Math.min(heater[4][i],temperature[2][(int)(currentTemp*10+1)]);
            } else {
                R2=0;
            }
            //R3
            if (temperature[3][(int)(currentTemp*10+1)]>0) {
                R3=Math.min(heater[3][i],temperature[3][(int)(currentTemp*10+1)]);
            } else {
                R3=0;
            }
            //R4
            if (temperature[4][(int)(currentTemp*10+1)]>0) {
                R4=Math.min(heater[2][i],temperature[4][(int)(currentTemp*10+1)]);
            } else {
                R4=0;
            }
            //R5
            if (temperature[5][(int)(currentTemp*10+1)]>0) {
                R5=Math.min(heater[1][i],temperature[5][(int)(currentTemp*10+1)]);
            } else {
                R5=0;
            }
            value[1][i]=Math.max(R1,Math.max(R2,Math.max(R3,Math.max(R4,R5))));
        }
        
        double l=0;
        double m=0;
        for(int i=0;i<1601;i++) {
            l+=(value[1][i]*value[0][i]);
            m+=value[1][i];
        }
        //System.out.println(l/m);
        //System.out.println((int) (((l/m)-50)*2.56));
        //System.out.println(currentTemp);
        
        if(l/m<0) {
            heatLevel=(l/m);
            currentTempChange=(heatLevel)/64.00;
        } else if (l/m>0) {
            heatLevel=(l/m);
            currentTempChange=(heatLevel)/64.00;
        } else {
            heatLevel=0;
            currentTempChange=0;
        }      
    }
    
    public void update() {
        
        if(location==0){
            if(tempChange>currentTemp){
                x=0.01;
            }else if(tempChange<currentTemp){
                x=-0.01;
            }else{
                x=0;
            }
        }else if(location==1){
            x=convertW2C(getQ(0.2, 36, currentTemp, tempChange),62400);
        }else if(location==2){
            x=convertW2C(getQ(1, 36, currentTemp, tempChange),62400);
        }else if(location==3){
            x=convertW2C(getQ(33.3, 64, currentTemp, tempChange),62400);
        }
        
        currentTemp+=(x+currentTempChange);
    }    
    
    public double getHeatLevel() {
        return heatLevel;
    }
    
    public double getCurrentTemp() {
        return currentTemp;
    }
    
    public double getCurrentTempChange() {
        return currentTempChange+x;
    }
    
    public void setLocation(int l) {
        location=l;
    }
    
    public void setTempChange(double tC) {
        tempChange=tC;
    }
    /*
    public double convertC2W(Double c){
        return ((c/60)*1.0035)*62400;
    }
    */
    public double convertW2C(double w, double s){
        return ((w/s)/1.0035)*60;
    }
    
    private double getQ(double u, double s, double cT, double tC) {
        return (u*s*(tC-cT))/60;
    }
}
